package com.project.project.Contorllers.AdminControllers;

import com.project.project.dto.AuthorDTO;
import com.project.project.errors.ExistByNameException;
import com.project.project.services.AuthorService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin/author")
public class AuthorController {

    private final AuthorService service;

    public AuthorController(AuthorService service) {
        this.service = service;
    }

    //получить всех авторов
    @GetMapping
    public String listAll(Model model){
        model.addAttribute("authors", service.listAll());
        return "AdminPages/listPages/authors";
    }

    //Получить форму для обновления автора
    @GetMapping("/update")
    public String getUpdateForm(@RequestParam Long id, Model model){
        AuthorDTO dto = service.getOne(id);
        model.addAttribute("author", dto);
        return "AdminPages/updatePages/author";
    }

    //Получить форму для добавления автора
    @GetMapping("/add")
    public String getAddForm(Model model){
        AuthorDTO dto = new AuthorDTO();
        model.addAttribute("author",dto);
        return "AdminPages/addPages/author";
    }

    //Удаление автора
    @GetMapping("/delete")
    public String delete(@RequestParam("id") Long id){
            service.delete(id);
            return "redirect:/admin/author";
    }

    //Добавления автора
    @PostMapping
    public String add(@Valid AuthorDTO dto, Model model, BindingResult result){
        if(result.hasErrors()){
            return "AdminPages/addPages/author";
    }
        try{
            service.addAuthor(dto);
        } catch (ExistByNameException ex) {
            //Если автор уже существует с таким именнем возвращается ошибка
            model.addAttribute("author", dto);
            model.addAttribute("error" , true);
            model.addAttribute("name", dto.getName());
            System.out.println(ex.getMessage());
            return "AdminPages/addPages/author";
        }
        return "redirect:/admin";
    }

    //Обновление автора
    @PostMapping("/update")
    public String update(AuthorDTO dto){
        service.update(dto.getName(), dto.getId());
        return "redirect:/admin/author";
    }
}
