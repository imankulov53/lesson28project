package com.project.project.Contorllers.AdminControllers;

import com.project.project.MappingService.MappingService;
import com.project.project.dto.AuthorDTO;
import com.project.project.dto.BookDTO;
import com.project.project.dto.LanguageDTO;
import com.project.project.dto.SectionDTO;
import com.project.project.entities.Author;
import com.project.project.entities.Language;
import com.project.project.entities.Section;
import com.project.project.errors.ExistByNameException;
import com.project.project.services.AuthorService;
import com.project.project.services.BookService;
import com.project.project.services.LanguageService;
import com.project.project.services.SectionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/admin/book")
public class BookController {

    private final BookService service;

    private final SectionService sectionService;

    private final AuthorService authorService;

    private final MappingService mappingService;

    private final LanguageService languageService;

    public BookController(BookService service, SectionService sectionService, AuthorService authorService, MappingService mappingService, LanguageService languageService) {
        this.service = service;
        this.sectionService = sectionService;
        this.authorService = authorService;
        this.mappingService = mappingService;
        this.languageService = languageService;
    }


    @ModelAttribute("sections")
    public List<SectionDTO> sections() {
        return sectionService.listAll();
    }

    @ModelAttribute("authors")
    public List<AuthorDTO> authors() {
        return authorService.listAll();
    }

    @ModelAttribute("books")
    public List<BookDTO> books(){return service.listAll();}

    @ModelAttribute("languages")
    public List<LanguageDTO> languages(){return languageService.listAll();}

    //Получить все книги
    @GetMapping
    public String getAll(){
        return "AdminPages/listPages/books";
    }
    //Получить форму для добавление книги
    @GetMapping("/add")
    public String getAddForm(Model model){
        BookDTO dto = new BookDTO();
        model.addAttribute("book",dto);
        return "AdminPages/addPages/book";
    }

    //Получить форму для обновление книги
    @GetMapping("/update")
    public String getUpdateForm(@RequestParam("id") Long id,Model model){
        model.addAttribute("book", service.getOne(id));
        return "AdminPages/updatePages/book";
    }

    //Удаление книги
    @GetMapping("/delete")
    public String delete(@RequestParam("id") Long id){
        service.delete(id);
        return "redirect:/admin/book";
    }

    //Добавление книги
    @PostMapping
    public String add(BookDTO dto, Model model){
        dto.setAuthor(authorService.getOne(dto.getAuthorId()));
        dto.setSection(sectionService.getOne(dto.getSectionId()));
        dto.setLanguage(languageService.getOne(dto.getLanguageId()));
        try {
            service.save(dto);
        } catch (ExistByNameException e) {
            //Если книга с таким именем и автором и языком существует, возвращется сообщение
            String message = dto.getName() + " Автор: "+dto.getAuthorName() + " Язык: " +dto.getLanguageName();
            model.addAttribute("book", dto);
            model.addAttribute("error" , true);
            model.addAttribute("message", message);
            return "AdminPages/addPages/book";
        }
        return "redirect:/admin";
    }

    //Обновление книги
    @PostMapping("/update")
    public String update(BookDTO dto){
        Section section = mappingService.mapToSection(sectionService.getOne(dto.getSectionId()));
        Author author = mappingService.mapToAuthor(authorService.getOne(dto.getAuthorId()));
        Language language = mappingService.mapToLanguage(languageService.getOne(dto.getLanguageId()));
        service.update(dto.getName(),section,author,language,dto.getPrice(),dto.getCount(),dto.getId());
        return "redirect:/admin/book";
    }

}
