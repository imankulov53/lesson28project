package com.project.project.Contorllers.AdminControllers;

import com.project.project.dto.LanguageDTO;
import com.project.project.errors.ExistByNameException;
import com.project.project.services.LanguageService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
@RequestMapping("admin/language")
public class LanguageController {

    private final LanguageService service;

    public LanguageController(LanguageService service) {
        this.service = service;
    }

    @GetMapping
    public String list(Model model){
        model.addAttribute("languages", service.listAll());
        return "AdminPages/listpages/language";
    }

    @GetMapping("/update")
    public String getUpdateForm(@RequestParam Long id, Model model){
        LanguageDTO dto = service.getOne(id);
        model.addAttribute("language", dto);
        return "AdminPages/updatePages/language";
    }

    @GetMapping("/add")
    public String getAddForm(Model model){
        LanguageDTO dto = new LanguageDTO();
        model.addAttribute("language",dto);
        return "AdminPages/addPages/language";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("id") Long id){
        service.delete(id);
        return "redirect:/admin/language";
    }
    @PostMapping
    public String add(@Valid LanguageDTO dto, Model model, BindingResult result){
        if(result.hasErrors()){
            return "AdminPages/addPages/language";
        }
        try{
            service.save(dto);
        } catch (ExistByNameException ex) {
            //Если существует такой язык возвращается ошибка
            model.addAttribute("language", dto);
            model.addAttribute("error" , true);
            model.addAttribute("name", dto.getName());
            System.out.println(ex.getMessage());
            return "AdminPages/addPages/language";
        }
        return "redirect:/admin";
    }

    @PostMapping("/update")
    public String update(LanguageDTO dto){
        service.update(dto.getName(), dto.getId());
        return "redirect:/admin/language";
    }

}
