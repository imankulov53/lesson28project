package com.project.project.Contorllers.AdminControllers;


import com.project.project.dto.AuthorDTO;
import com.project.project.dto.SectionDTO;
import com.project.project.errors.ExistByNameException;
import com.project.project.services.SectionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/admin/section")
public class SectionController {

    private final SectionService service;

    public SectionController(SectionService service) {
        this.service = service;
    }

    //получить всех разделы
    @GetMapping
    public String getAll(Model model){
        model.addAttribute("sections", service.listAll());
        return "AdminPages/listPages/sections";
    }

    //Получение формы для обновление раздела
    @GetMapping("/update")
    public String getUpdateForm(@RequestParam Long id, Model model){
        SectionDTO dto = service.getOne(id);
        model.addAttribute("section", dto);
        return "AdminPages/updatePages/section";
    }

    //Получить форму для добавления раздела
    @GetMapping("/add")
    public String getAddForm(Model model){
        SectionDTO dto = new SectionDTO();
        model.addAttribute("section", dto);
        return "AdminPages/addPages/section";
    }

    //Удаление разделов
    @GetMapping("/delete")
    public String delete(@RequestParam("id") Long id){
        service.delete(id);
        return "redirect:/admin/section";
    }

    //Добавления раздела
    @PostMapping
    public String add(SectionDTO dto, Model model){
        try {
            service.addSection(dto);
        } catch (ExistByNameException e) {
            //Если раздел такой уже существет возвращается ошибка
            model.addAttribute("section", dto);
            model.addAttribute("error" , true);
            model.addAttribute("name", dto.getName());
            return "AdminPages/addPages/section";
        }
        return "redirect:/admin";
    }

    //Обновление раздела
    @PostMapping("/update")
    public String update(AuthorDTO dto){
        service.update(dto.getName(), dto.getId());
        return "redirect:/admin/section";
    }
}
