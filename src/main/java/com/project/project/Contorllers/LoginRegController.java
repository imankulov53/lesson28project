package com.project.project.Contorllers;

import com.project.project.dto.UserDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class LoginRegController {
    //Получаем стринцу логина
    @GetMapping("/login")
    public String getLogin(Model model){
        UserDTO dto = new UserDTO();
        model.addAttribute("user",dto);
        model.addAttribute("loginError",false);
        return "LoginPages/login";
    }

    //Получаем стринцу логина которая высвечивает информацию о ошибке со входом
    @GetMapping("/login-error")
    public String getErrorMessage(Model model){
        UserDTO dto = new UserDTO();
        model.addAttribute("user",dto);
        model.addAttribute("loginError",true);
        return "LoginPages/login";
    }
}
