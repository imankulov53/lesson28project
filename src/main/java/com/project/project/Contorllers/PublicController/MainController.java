package com.project.project.Contorllers.PublicController;

import com.project.project.MappingService.MappingService;
import com.project.project.dto.*;
import com.project.project.entities.Author;
import com.project.project.entities.Language;
import com.project.project.entities.Section;
import com.project.project.services.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/public")
public class MainController {
    private static int countMode =1;

    private final UsersService usersService;
    private final BookService bookService;
    private final SectionService sectionService;
    private final BasketService basketService;
    private final AuthorService authorService;
    private final MappingService mappingService;
    private final LanguageService languageService;

    public MainController(UsersService usersService, BookService bookService, AuthorService authorService, SectionService sectionService, BasketService basketService, MappingService mappingService, LanguageService languageService) {
        this.usersService = usersService;
        this.bookService = bookService;
        this.authorService = authorService;
        this.sectionService = sectionService;
        this.basketService = basketService;
        this.mappingService = mappingService;
        this.languageService = languageService;
    }
        /*
        Используются атрибуты модели для отображения на странице
         */
    @ModelAttribute("sections")
    public List<SectionDTO> sections() {
        return sectionService.listAll();
    }

    @ModelAttribute("authors")
    public List<AuthorDTO> authors() {
        return authorService.listAll();
    }

    @ModelAttribute("languages")
    public List<LanguageDTO> languages() {
        return languageService.listAll();
    }

    //Получаем список книг и разделов
    @GetMapping
    public String FirstPage(Model model, FilterObjectDTO filterObject) {
        model.addAttribute("orderBy", filterObject.getOrderBy());
        model.addAttribute("mode", switchMode(filterObject.getMode()));
        model.addAttribute("books", filteredList(filterObject));
        return getMainPage(model, filterObject);
    }
    //Получаем книги согласно разделу выбранному с левой колонки разделов
    @GetMapping("/section")
    public String bookBySection(@RequestParam("id")Long id, Model model,FilterObjectDTO filterObject){
        if (id!=null)
        {
            filterObject.setSectionId(id);
        }
        SectionDTO sectionDTO = sectionService.getOne(id);
        model.addAttribute("books",bookService.listBookBySection(sectionDTO));
        return getMainPage(model, filterObject);
    }

    //Получаем страницу для регистрации
    @GetMapping("/registration")
    public String getRegister(Model model) {
        UserDTO user = new UserDTO();
        model.addAttribute("user", user);
        model.addAttribute("notMatch",false);
        return "LoginPages/registration";
    }

    //Регистрируем пользователя
    @PostMapping("/registration")
    public String register(UserDTO user,Model model) {
        user.setRoles("ROLE_USER");
        if (user.getPassword().equals(user.getConfirmedPassword())){
            usersService.save(user);
        }else {
            //Если пароли не совпадают выдается соответсвующая ошибка
            model.addAttribute("user",user);
            model.addAttribute("notMatch", true);
            return "LoginPages/registration";
        }
        //Для каждого зарегистрированного юзера автоматически создается пустая корзина
        BasketDTO dto = new BasketDTO();
        dto.setUserDTO(mappingService.mapToUserDto(usersService.getOne(user.getUsername())));
        dto.setBookList(new ArrayList<>());
        basketService.save(dto);
        return "redirect:/public";
    }

    //Метод который возращает главную страницу
    private String getMainPage(Model model, FilterObjectDTO filterObject) {
        Object user = getCurrentUser();
        if (user instanceof UserDetails) {
            String username = ((UserDetails) user).getUsername();
            model.addAttribute("username", username);
            model.addAttribute("isAuthenticated", true);
        } else
        {
            model.addAttribute("username", "Guest");
            model.addAttribute("isAuthenticated", false);
        }

        model.addAttribute("filterObject", filterObject);
        return "mainpage";
    }

    //Возвращает текущего авторизированного юзера
    private Object getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object user = auth.getPrincipal();
        return user;
    }

    //Используется для смены модов сортировки, также в случае если подоходит к 3-ей итерации возвращает мод в начальное положение asc
    //Проверяется кол.во итерацией переменной countMode
    private String switchMode(String mode) {
        String newMode = "desc";
        if (countMode==3){
            newMode = "asc";
            countMode=1;
        }else {
            countMode+=1;
            if (mode.equals("desc"))
                newMode = "asc";
        }
        return newMode;
    }

    //Получение книг согласно параметром фильтрации
    private List<BookDTO> filteredList(FilterObjectDTO filterObject){
        Section section;
        Author author;
        Language language;
        if (filterObject.getSectionId()==null){
            section = null;
        } else
            section = mappingService.mapToSection(sectionService.getOne(filterObject.getSectionId()));
        if (filterObject.getAuthorId()==null){
            author =null;
        } else author = mappingService.mapToAuthor(authorService.getOne(filterObject.getAuthorId()));

        if (filterObject.getLanguageId()==null){
            language =null;
        } else language = mappingService.mapToLanguage(languageService.getOne(filterObject.getLanguageId()));

        List<BookDTO> books = bookService.
                listAll(section,
                        filterObject.getAmountFrom(),
                        filterObject.getAmountTo(),
                        author,
                        language,filterObject.getSearch(),
                        filterObject.getOrderBy(),filterObject.getMode());
        return books;
    }
}
