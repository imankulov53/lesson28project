package com.project.project.Contorllers.UserController;


import com.project.project.MappingService.MappingService;
import com.project.project.dto.BasketDTO;
import com.project.project.dto.BookDTO;
import com.project.project.entities.Users;
import com.project.project.services.BasketService;
import com.project.project.services.BookService;
import com.project.project.services.UsersService;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/user/basket")
public class BasketController {

    private final BookService bookService;

    private final MappingService mappingService;

    private final BasketService basketService;

    private final UsersService usersService;

    public BasketController(BookService bookService, MappingService mappingService, BasketService basketService, UsersService usersService) {
        this.bookService = bookService;
        this.mappingService = mappingService;
        this.basketService = basketService;
        this.usersService = usersService;
    }

    //Происходит добавление одной книги в корзину авторизированного юзера
    //Меняя заранее значение в БД, проще говоря "бронирует" книгу
    @GetMapping
    public String addBookToBasket(@Param("id") Long id) {

        BookDTO bookDTO = bookService.getOne(id);
        bookService.updateCount(bookDTO.getCount()-1,id);
        BasketDTO basketDTO = getBasketFromCurrentUser();
        List<BookDTO> list = getBookList();
        list.add(bookDTO);
        basketDTO.setOrderTime(LocalDateTime.now());

        basketDTO.setBookList(list);

        basketService.save(basketDTO);
        return "redirect:/public";
    }

    /*Просиходит отображение книг в корзине, с учетом количество повторяющихся книг
    Дополнительно параметры:
    sum = общая стоимость всех книг в корзине, в случае если sum==0 выводится соответствующее сообщение
    bough = отвечает за момент совершения заказа, в случае если заказ оформлен, то выводится соответствующее сообщение*/
    @GetMapping("/show")
    public String show(Model model) {
        int sum = getBookList().stream().mapToInt(BookDTO::getPrice).sum();
        HashMap<BookDTO, Long> books = getFilteredList(getBasketFromCurrentUser());
        model.addAttribute("sum",sum);
        model.addAttribute("books",books);
        model.addAttribute("bought",false);
        return "UserPages/showBasket";
    }
    //Удаляет по одной книги из коризны текущего юзера
    @GetMapping("/remove")
     public String remove(@RequestParam("id") Long id){
        BookDTO bookDTO = bookService.getOne(id);
        bookService.updateCount(bookDTO.getCount()+1,id);
        BasketDTO basketDTO = getBasketFromCurrentUser();
        List<BookDTO> list = getBookList();
        list.remove(bookDTO);
        basketDTO.setBookList(list);
        basketService.save(basketDTO);
        return "redirect:/user/basket/show";
    }

    /*
    Происходит оформления заказа, перевод статуса текущей корзины юзера в sold=true
    После создается новая пустая корзина и присваевается к текущему юзеру, сохраняется в БД*/
    @GetMapping("/buy")
    public String buy(Model model){
        BasketDTO basketDTO = new BasketDTO();
        if (!getBookList().isEmpty()) {
            basketService.updateSold(true, getBasketFromCurrentUser().getId());
            LocalDateTime dateTime = LocalDateTime.now();
            basketDTO.setUserDTO(mappingService.mapToUserDto(usersService.getOne(getCurrentUser().getUsername())));
            basketDTO.setBookList(new ArrayList<>());
            basketDTO.setOrderTime(dateTime);
            basketService.save(basketDTO);
        }
        model.addAttribute("bought",true);
        return "UserPages/showBasket";
    }

    /*Повторение прошлого заказа
    Работает по след логике, берется текущая корзина юзера и добавляются книги со старой(sold==true) корзины, автоматически "бронируя" кол.во книг из БД
    Сделал для того, чтобы кол.во книг считалось заранее "бронировалось".
    */
    @GetMapping("/repeat")
    public String addToBasket(@Param("id") Long id){
        BasketDTO sold = mappingService.mapToBasketDto(basketService.getOne(id));
        HashMap<BookDTO,Long> books = getFilteredList(sold);

        BasketDTO dto = getBasketFromCurrentUser();
        List<BookDTO> list = getBookList();
        sold.getBookList().forEach(b -> list.add(b));

        for (Map.Entry<BookDTO,Long> book:
             books.entrySet()) {
                bookService.updateCount((int) (book.getKey().getCount()-book.getValue()),book.getKey().getId());
        }
            dto.setBookList(list);
            basketService.save(dto);
        return "redirect:/user/basket/show";
    }

    /*Отображение всех совершенных заказов(sold==true) текущего юзера
    Дополнительно проверяется можно ли повторить заказ полностью.
    Параметр bought= говорит о возможности повтора заказа с тем же количеством книг, если параметр bought==false.
    Иначе заказ нельзя будет повторить полностью, юзеру, необходимо выбрать другое кол.во книг,
    которые можно добавить по одной в текущую активную(sold==false) корзину юзера*/
    @GetMapping("/history")
    public String oldBaskets(Model model){
        List<BasketDTO> list = basketService.findSold(getCurrentUser());
        HashMap<BasketDTO, Map<?,?>> baskets = new HashMap<>();
        for (int i = 0; i <list.size() ; i++) {
            HashMap<BookDTO,Long> books = getFilteredList(list.get(i));
            for (Map.Entry<BookDTO, Long> entry:
                 books.entrySet()) {
                if (bookService.getOne(entry.getKey().getId()).getCount()<entry.getValue()){
                    list.get(i).setBought(true);
                }
            }
            baskets.put(list.get(i),books);
        }
        model.addAttribute("baskets", baskets);
        return "UserPages/historyBasket";
    }

    //получение текущей корзины юзера
    private BasketDTO getBasketFromCurrentUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Users user = usersService.getOne(((UserDetails) auth.getPrincipal()).getUsername());
        BasketDTO basketDTO = basketService.getOne(user);
        return basketDTO;
    }
    //получение списка книг в текущей коризны юзера
    private List<BookDTO> getBookList(){
        return getBasketFromCurrentUser().getBookList();
    }
    //получение текущего юзера
    private Users getCurrentUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Users user = usersService.getOne(((UserDetails) auth.getPrincipal()).getUsername());
        return user;
    }
    //Получение группы книг и их кол.во
    private HashMap<BookDTO, Long> getFilteredList(BasketDTO basket){
        HashMap<BookDTO,Long> books = new HashMap<>();
        basket.getBookList().stream().collect(Collectors.groupingBy(Function.identity(),Collectors.counting()))
                .entrySet().forEach(bookDTOLongEntry -> books.put(bookDTOLongEntry.getKey(),bookDTOLongEntry.getValue()));
        return books;
    }
}
