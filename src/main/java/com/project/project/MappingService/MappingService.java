package com.project.project.MappingService;

import com.project.project.dto.*;
import com.project.project.entities.*;
import org.springframework.stereotype.Service;;
import java.util.stream.Collectors;

@Service
public class MappingService {

    //Класс с методами перевода объекта с DTO в entity и наоборот

    public BookDTO mapToBookDto(Book entity) {
        BookDTO dto = new BookDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setSection(mapToSectionDto(entity.getSection()));
        dto.setAuthor(mapToAuthorDto(entity.getAuthor()));
        dto.setLanguage(mapToLanguageDto(entity.getLanguage()));
        dto.setPrice(entity.getPrice());
        dto.setCount(entity.getCount());
        return dto;
    }

    public Book mapToBook(BookDTO dto){
        Book entity = new Book();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setSection(mapToSection(dto.getSection()));
        entity.setAuthor(mapToAuthor(dto.getAuthor()));
        entity.setLanguage(mapToLanguage(dto.getLanguage()));
        entity.setPrice(dto.getPrice());
        entity.setCount(dto.getCount());
        return entity;
    }

    public AuthorDTO mapToAuthorDto(Author entity){
        AuthorDTO dto = new AuthorDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        return dto;
    }

    public Author mapToAuthor(AuthorDTO dto){
        Author entity = new Author();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        return entity;
    }

    public SectionDTO mapToSectionDto(Section entity){
        SectionDTO dto = new SectionDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        return dto;
    }

    public Section mapToSection(SectionDTO dto){
        Section entity = new Section();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        return entity;
    }

    public BasketDTO mapToBasketDto(Basket entity){
        BasketDTO dto = new BasketDTO();
        dto.setId(entity.getId());
        dto.setOrderTime(entity.getOrderTime());
        dto.setUserDTO(mapToUserDto(entity.getUser()));
        dto.setBookList(entity.getBooks().stream().map(this::mapToBookDto).collect(Collectors.toList()));
        dto.setSold(entity.isSold());
        return dto;
    }

    public Basket mapToBasket(BasketDTO dto){
        Basket entity = new Basket();
        entity.setId(dto.getId());
        entity.setOrderTime(dto.getOrderTime());
        entity.setUser(mapToUser(dto.getUserDTO()));
        entity.setBooks(dto.getBookList().stream().map(this::mapToBook).collect(Collectors.toList()));
        entity.setSold(dto.isSold());
        return entity;
    }

    public UserDTO mapToUserDto(Users entity){
        UserDTO dto = new UserDTO();
        dto.setId(entity.getId());
        dto.setUsername(entity.getUsername());
        dto.setPassword(entity.getPassword());
        dto.setRoles(entity.getRoles());
        return dto;
    }

    public Users mapToUser(UserDTO dto){
        Users entity = new Users();
        entity.setId(dto.getId());
        entity.setUsername(dto.getUsername());
        entity.setPassword(dto.getPassword());
        entity.setRoles(dto.getRoles());
        return entity;
    }

    public Language mapToLanguage(LanguageDTO dto){
        Language entity = new Language();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        return entity;
    }

    public LanguageDTO mapToLanguageDto(Language entity){
        LanguageDTO dto = new LanguageDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        return dto;
    }
}
