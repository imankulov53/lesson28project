package com.project.project.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
public class AuthorDTO {
    private Long id;

    @NotBlank(message = "Имя не может быть пустым")
    private String name;
}
