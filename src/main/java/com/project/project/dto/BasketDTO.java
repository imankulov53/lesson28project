package com.project.project.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import java.util.List;


@Data
@NoArgsConstructor
public class BasketDTO {

    private Long id;
    private LocalDateTime orderTime;
    private List<BookDTO> bookList;
    private UserDTO userDTO;
    private boolean sold;
    private boolean bought;
}
