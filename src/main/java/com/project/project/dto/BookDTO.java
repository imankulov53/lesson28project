package com.project.project.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Objects;

@Data
@NoArgsConstructor
public class BookDTO implements Comparable
{

    //Эти переменные используются для передачи информацию в бд
    private Long id;

    @NotBlank(message = "Имя не может быть пустым")
    private String name;

    @NotNull(message = "Укажите раздел")
    private SectionDTO section;
    @NotNull(message = "Укажите автора")
    private AuthorDTO author;
    @NotBlank(message = "Укажите язык")
    private LanguageDTO language;
    @Positive(message = "Цена не может быть меньше или равна нулю")
    private int price;
    private int count;

    //Эти переменные используются для передачи информации клиенту
    private String sectionName;
    private String authorName;
    private String languageName;
    private Long sectionId;
    private Long authorId;
    private Long languageId;

    public String getLanguageName() {
        return language.getName();
    }

    public String getSectionName() {
        return sectionName =section.getName();
    }

    public String getAuthorName() {
        return authorName = author.getName();
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookDTO dto = (BookDTO) o;
        return id.equals(dto.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public int compareTo(Object o) {
        if(this.getId()==((BookDTO)o).getId()){
            return 0;
        }else{
            if (this.getId()<((BookDTO)o).getId()){
                return -1;
            }else return 1;
        }
    }
}
