package com.project.project.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FilterObjectDTO {

    private String orderBy = "id";
    private String mode = "asc";
    private int amountFrom = 0;
    private int amountTo = 10000000;
    private Long sectionId = null;
    private Long authorId = null;
    private Long languageId = null;
    private String search = null;

    private String placeholder = "Search";
}
