package com.project.project.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LanguageDTO {

    private Long id;
    private String name;
}
