package com.project.project.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
public class SectionDTO {

    private Long id;

    @NotBlank(message = "Раздел не может быть пустым")
    private String name;
}
