package com.project.project.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
public class UserDTO {

    private Long id;
    @NotBlank(message = "Укажите логин")
    private String username;
    @NotBlank(message = "Укажите парль")
    private String password;
    private String confirmedPassword;
    private String roles;

}
