package com.project.project.entities;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "author")
@Data
@NoArgsConstructor
public class Author {

    @Id
    @SequenceGenerator(name = "authorID", sequenceName = "authorID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "authorID")
    private Long id;

    private String name;

    @OneToMany(mappedBy = "author", cascade = CascadeType.REMOVE)
    List<Book> listAuthor;
}
