package com.project.project.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "basket")
@Data
@NoArgsConstructor
public class Basket {

    @Id
    @SequenceGenerator(name = "basketID", sequenceName = "basketID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "basketID")
    private Long id;

    @Column(name = "order_time")
    private LocalDateTime orderTime;

    @ManyToMany
    @JoinTable(
            name = "book_basket",
            joinColumns = {@JoinColumn(name = "id_basket")},
            inverseJoinColumns = {@JoinColumn(name = "id_book")}
    )
    private List<Book> books;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users user;

    private boolean sold;
}
