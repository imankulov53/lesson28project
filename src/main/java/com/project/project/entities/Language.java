package com.project.project.entities;

import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "languages")
@Data
@NoArgsConstructor
public class Language {

    @Id
    @SequenceGenerator(name = "languageID", sequenceName = "languageID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "languageID")
    private Long id;

    private String name;

    @OneToMany(mappedBy = "language", cascade = CascadeType.REMOVE)
    List<Book> listLanguage;
}
