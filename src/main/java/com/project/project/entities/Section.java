package com.project.project.entities;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "section")
@Data
@NoArgsConstructor
public class Section {

    @Id
    @SequenceGenerator(name = "sectionID", sequenceName = "sectionID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sectionID")
    private Long id;


    private String name;

    @OneToMany(mappedBy = "section",  cascade = CascadeType.REMOVE)
    private List<Book> bookList;
}
