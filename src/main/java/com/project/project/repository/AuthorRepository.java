package com.project.project.repository;

import com.project.project.entities.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends JpaRepository<Author,Long> {
    @Modifying
    @Query("update Author a set a.name=:name where a.id=:id")
    void update(@Param("name") String name, @Param("id") Long id);

    @Query("SELECT a FROM Author a WHERE lower(a.name) = lower(:name) ")
    Author findByName(String name);
}
