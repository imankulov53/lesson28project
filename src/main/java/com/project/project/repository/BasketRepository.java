package com.project.project.repository;

import com.project.project.entities.Basket;
import com.project.project.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BasketRepository  extends JpaRepository<Basket,Long> {

    @Query("Select b from Basket b where b.user=:user and b.sold=false")
    Basket findBasketByUser(Users user);

    @Modifying
    @Query("update Basket b set b.sold=:sold where b.id=:id")
    void update(@Param("sold") boolean sold,
                    @Param("id") Long id);

    @Query("Select b from Basket b where b.user=:user and b.sold=true ")
    List<Basket> findSold(Users user);
}
