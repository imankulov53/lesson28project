package com.project.project.repository;

import com.project.project.dto.BookDTO;
import com.project.project.dto.SectionDTO;
import com.project.project.entities.Author;
import com.project.project.entities.Book;
import com.project.project.entities.Language;
import com.project.project.entities.Section;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book,Long> {

    @Modifying
    @Query("update Book b set b.name=:name, b.section=:section,b.author=:author,b.language=:language,b.price=:price, b.count=:count where b.id=:id")
    void update(@Param("name") String name,
                @Param("section") Section section,
                @Param("author") Author author,
                @Param("language") Language language,
                @Param("price") int price,
                @Param("count") int count,
                @Param("id") Long id);

    @Modifying
    @Query("update Book b set b.count=:count where b.id=:id")
    void update(@Param("count") int count,
                @Param("id") Long id);

    @Query("SELECT b FROM Book b WHERE lower(b.name) = lower(:name) AND b.author =:author AND b.language=:language ")
    Book findExisted(String name,
                    Author author,
                    Language language);

    @Query("SELECT b FROM Book b WHERE " +
            "trim(lower(b.name)) LIKE trim(lower(:search)) OR trim(lower(b.name)) like trim(lower(:regular)) " +
            "AND(:sectionId IS NULL OR b.section=:sectionId) " +
            "AND (:author IS NULL OR b.author=:author) " +
            "AND (:language IS NULL OR b.language=:language) " +
            "AND (:amountFrom IS NULL OR b.price>=:amountFrom ) "+
            "AND (:amountTo IS NULL OR b.price<=:amountTo)")
    List<Book> findAll(
            @Param("sectionId") Section sectionId,
            @Param("language") Language language,
            @Param("amountFrom") int amountFrom,
            @Param("amountTo") int amountTo,
            @Param("author") Author author,
            @Param("search") String search,
            @Param("regular") String regular,
            Sort sort);

    List<Book> findBySection(Section section);
}
