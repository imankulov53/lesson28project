package com.project.project.repository;

import com.project.project.entities.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LanguageRepository extends JpaRepository<Language, Long> {

    @Modifying
    @Query("update Language l set l.name=:name where l.id=:id")
    void update(@Param("name") String name, @Param("id") Long id);

    @Query("SELECT l FROM Language l WHERE lower(l.name) = lower(:name) ")
    Language findByName(String name);
}
