package com.project.project.repository;

import com.project.project.entities.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SectionRepository extends JpaRepository<Section,Long> {

    @Modifying
    @Query("update Section a set a.name=:name where a.id=:id")
    void update(@Param("name") String name, @Param("id") Long id);

    @Query("SELECT s FROM Section s WHERE lower(s.name) = lower(:name) ")
    Section findByName(String name);
}
