package com.project.project.services;

import com.project.project.MappingService.MappingService;
import com.project.project.dto.AuthorDTO;
import com.project.project.entities.Author;
import com.project.project.errors.ExistByNameException;
import com.project.project.repository.AuthorRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthorService {

    private final AuthorRepository repository;

    private final MappingService mappingService;

    public AuthorService(AuthorRepository repository, MappingService mappingService) {
        this.repository = repository;
        this.mappingService = mappingService;
    }

    public List<AuthorDTO> listAll() {
        return repository.findAll().stream().map(mappingService::mapToAuthorDto).collect(Collectors.toList());
    }

    public void addAuthor(AuthorDTO dto) throws ExistByNameException {
        Author entity = mappingService.mapToAuthor(dto);
        entity.setName(entity.getName().trim());

        if (repository.findByName(entity.getName())!=null) throw new ExistByNameException();

        repository.save(entity);
    }

    public AuthorDTO getOne(Long id) {
        return mappingService.mapToAuthorDto(repository.getOne(id));
    }

    @Transactional
    public void update(String name, Long id) {
        repository.update(name, id);
    }

    public void delete(Long id){
        repository.deleteById(id);
    }
}
