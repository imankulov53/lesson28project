package com.project.project.services;

import com.project.project.MappingService.MappingService;
import com.project.project.dto.BasketDTO;
import com.project.project.entities.Basket;
import com.project.project.entities.Users;
import com.project.project.repository.BasketRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BasketService {

        private final BasketRepository repository;
        private final MappingService mappingService;

        public BasketService(BasketRepository repository, MappingService mappingService) {
                this.repository = repository;
                this.mappingService = mappingService;
        }

        public void save(BasketDTO basket){
                repository.save(mappingService.mapToBasket(basket));
        }

        public Basket getOne(Long id){
                return repository.getOne(id);
        }

        public BasketDTO getOne(Users user){ return mappingService.mapToBasketDto(repository.findBasketByUser(user));}

        @Transactional
        public void updateSold(boolean sold, Long id){
                repository.update(sold,id);
        }

        public List<BasketDTO> findSold(Users user){
                return repository.findSold(user).stream().map(mappingService::mapToBasketDto).collect(Collectors.toList());
        }

}
