package com.project.project.services;

import com.project.project.MappingService.MappingService;
import com.project.project.dto.BookDTO;
import com.project.project.dto.SectionDTO;
import com.project.project.entities.Author;
import com.project.project.entities.Book;
import com.project.project.entities.Language;
import com.project.project.entities.Section;
import com.project.project.errors.ExistByNameException;
import com.project.project.repository.BookRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {
    private static int countMode = 1;

    private final BookRepository repository;

    private final MappingService mappingService;

    public BookService(BookRepository repository, MappingService mappingService) {
        this.repository = repository;
        this.mappingService = mappingService;
    }

    public List<BookDTO> listAll(){
        return repository.findAll().stream().map(mappingService::mapToBookDto).collect(Collectors.toList());
    }
    public void save(BookDTO dto)throws ExistByNameException {
        Book entity = mappingService.mapToBook(dto);
        entity.setName(entity.getName().trim());
        if (repository.
                findExisted(entity.getName(),entity.getAuthor(),entity.getLanguage()) !=null)
            throw new ExistByNameException();

        repository.save(entity);
    }

    public BookDTO getOne(Long id){
        return mappingService.mapToBookDto(repository.getOne(id));
    }

    @Transactional
    public void update(String name,
                       Section section,
                       Author author,
                       Language Language,
                       int price,
                       int count,
                       Long id) {

        repository.update(name, section, author, Language, price, count, id);
    }

    @Transactional
    public void updateCount(int count,
                       Long id){
        repository.update(count,id);
    }

    public void delete(Long id){
        repository.deleteById(id);
    }

    public List<BookDTO> listAll(Section sectionID,
                                 int amountFrom,
                                 int amountTo,
                                 Author author,
                                 Language language,
                                 String search,
                                 String orderBy,
                                 String mode) {
        String regular;
        if (search==null){
            search="%%";
            regular="%%";
        }else {
        regular = "%"+search+"%";
        }
        return repository.findAll(
                sectionID,
                language,
                amountFrom,
                amountTo,
                author,
                search,
                regular,
                defineMode(orderBy,mode))
                .stream().map(mappingService::mapToBookDto).collect(Collectors.toList());

    }
    /*Метод определяющий как производит сортировку.
    Дополнительно на каждую 3 операцию, возвращает список в исходное положение*/
    private Sort defineMode(String orderBy, String mode) {
        Sort sort;
        if(countMode==3){
            sort = Sort.by("id").ascending();
            countMode=1;
        }else{
            countMode+=1;
            sort= Sort.by(orderBy);
            if(mode.equals("desc")){
                sort = sort.descending();
            }else sort.ascending();
        }
        return sort;
    }

    public List<BookDTO> listBookBySection(SectionDTO sectionDTO) {
        return repository.findBySection(mappingService.mapToSection(sectionDTO)).stream().map(mappingService::mapToBookDto).collect(Collectors.toList());
    }
}
