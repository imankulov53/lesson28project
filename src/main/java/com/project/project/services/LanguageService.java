package com.project.project.services;

import com.project.project.MappingService.MappingService;
import com.project.project.dto.LanguageDTO;
import com.project.project.entities.Language;
import com.project.project.errors.ExistByNameException;
import com.project.project.repository.LanguageRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LanguageService{

    private final LanguageRepository repository;

    private final MappingService mappingService;

    public LanguageService(LanguageRepository repository, MappingService mappingService) {
        this.repository = repository;
        this.mappingService = mappingService;
    }

    public void save(LanguageDTO dto) throws ExistByNameException {
        Language entity = mappingService.mapToLanguage(dto);
        entity.setName(entity.getName().trim());

        if (repository.findByName(entity.getName())!=null) throw new ExistByNameException();

        repository.save(entity);
    }

    public List<LanguageDTO> listAll() {
        return repository.findAll().stream().map(mappingService::mapToLanguageDto).collect(Collectors.toList());
    }

    public LanguageDTO getOne(Long id) {
        return mappingService.mapToLanguageDto(repository.getOne(id));
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Transactional
    public void update(String name, Long id) {
        repository.update(name,id);
    }
}
