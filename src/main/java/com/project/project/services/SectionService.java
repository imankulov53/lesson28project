package com.project.project.services;


import com.project.project.MappingService.MappingService;
import com.project.project.dto.SectionDTO;
import com.project.project.entities.Section;
import com.project.project.errors.ExistByNameException;
import com.project.project.repository.SectionRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SectionService {

    private final SectionRepository repository;

    private final MappingService mappingService;

    public SectionService(SectionRepository repository, MappingService mappingService) {
        this.repository = repository;
        this.mappingService = mappingService;
    }

    public List<SectionDTO> listAll(){
        return repository.findAll().stream().map(mappingService::mapToSectionDto).collect(Collectors.toList());
    }

    public void addSection(SectionDTO dto) throws ExistByNameException{
        Section entity = mappingService.mapToSection(dto);
        entity.setName(entity.getName().trim());

        if (repository.findByName(entity.getName())!=null) throw new ExistByNameException();

        repository.save(entity);;
    }

    public SectionDTO getOne(Long id){
        return mappingService.mapToSectionDto(repository.getOne(id));
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Transactional
    public void update(String name, Long id) {
        repository.update(name, id);
    }
}
