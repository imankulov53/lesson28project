package com.project.project.services;

import com.project.project.MappingService.MappingService;
import com.project.project.dto.UserDTO;
import com.project.project.entities.Users;
import com.project.project.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class UsersService implements UserDetailsService {


    private PasswordEncoder encoder = new BCryptPasswordEncoder();

    @Autowired
    private UsersRepository repository;

    @Autowired
    private MappingService mappingService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users entity = repository.findByUsername(username);

        if (entity==null) throw new UsernameNotFoundException("username = " + username + " is not existed");

        ArrayList<SimpleGrantedAuthority> authorities = new ArrayList<>();

        authorities.add(new SimpleGrantedAuthority(entity.getRoles()));

        return new User(entity.getUsername(),entity.getPassword(),authorities);
    }

    public void save(UserDTO userDTO){
        Users entity = mappingService.mapToUser(userDTO);
        entity.setPassword(encoder.encode(entity.getPassword()));
        repository.saveAndFlush(entity);
    }

    public Users getOne(String username){
        return repository.findByUsername(username);
    }
}
