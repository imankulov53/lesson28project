CREATE SEQUENCE bookID;
CREATE SEQUENCE sectionID;
CREATE SEQUENCE authorID;
CREATE SEQUENCE basketID;
CREATE SEQUENCE languageID;

CREATE TABLE languages(
    id BIGINT DEFAULT NEXTVAL('languageID') PRIMARY KEY,
    name VARCHAR NOT NULL UNIQUE
);


CREATE TABLE section
(
    id   BIGINT DEFAULT NEXTVAL('sectionID') PRIMARY KEY,
    name varchar(50) NOT NULL UNIQUE
);

CREATE TABLE author
(
    id   BIGINT DEFAULT NEXTVAL('authorID') PRIMARY KEY,
    name varchar(50) NOT NULL UNIQUE
);
CREATE TABLE users
(

    id       BIGSERIAL PRIMARY KEY,
    username varchar UNIQUE,
    password varchar NOT NULL,
    roles    varchar not null
);


CREATE TABLE basket
(
    id         BIGINT PRIMARY KEY,
    order_time TIMESTAMP DEFAULT NOW(),
    user_id    BIGINT,
    FOREIGN KEY (user_id) REFERENCES users (id),
    sold       boolean
);

CREATE TABLE book
(
    id            BIGINT DEFAULT nextval('bookID') PRIMARY KEY,
    name          varchar(50) NOT NULL,
    section       BIGINT,
    FOREIGN KEY (section) REFERENCES section (id),
    author        BIGINT,
    FOREIGN KEY (author) REFERENCES author (id),
    language       BIGINT,
    FOREIGN KEY (language) REFERENCES languages(id),
    price         int         NOT NULL
);

CREATE TABLE book_basket
(
    id_book   BIGINT,
    FOREIGN KEY (id_book) REFERENCES book (id),
    id_basket BIGINT,
    FOREIGN KEY (id_basket) REFERENCES basket (id)
);
ALTER SEQUENCE bookID OWNED BY book.id;
ALTER SEQUENCE sectionId OWNED BY section.id;
ALTER SEQUENCE authorID OWNED BY author.id;
ALTER SEQUENCE languageID OWNED BY languages.id;



